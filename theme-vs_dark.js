ace.define('ace/theme/vs_dark', ['require', 'exports', 'module', 'ace/lib/dom'], function (acequire, exports, module) {
exports.isDark = false
exports.cssClass = 'ace-vs_dark'
exports.cssText = '.ace-vs_dark .ace_gutter {\
background: #1e1e1e;\
color: rgb(123,123,116)\
}\
\
.ace-vs_dark .ace_print-margin {\
width: 1px;\
background: #e8e8e8\
}\
\
.ace-vs_dark {\
background-color: #1e1e1e;\
color: #E6E1CF\
}\
\
.ace-vs_dark .ace_cursor {\
color: #F29718\
}\
\
.ace-vs_dark .ace_marker-layer .ace_selection {\
background: #253340\
}\
\
.ace-vs_dark.ace_multiselect .ace_selection.ace_start {\
box-shadow: 0 0 3px 0px #1e1e1e;\
border-radius: 2px\
}\
\
.ace-vs_dark .ace_marker-layer .ace_step {\
background: rgb(198, 219, 174)\
}\
\
.ace-vs_dark .ace_marker-layer .ace_bracket {\
margin: -1px 0 0 -1px;\
border: 1px solid #FFC107\
}\
\
.ace-vs_dark .ace_marker-layer .ace_active-line {\
background: #151A1F\
}\
\
.ace-vs_dark .ace_gutter-active-line {\
background-color: #151A1F\
}\
\
.ace-vs_dark .ace_marker-layer .ace_selected-word {\
border: 1px solid #253340\
}\
\
.ace-vs_dark .ace_fold {\
background-color: #FFB454;\
border-color: #E6E1CF\
}\
\
.ace-vs_dark .ace_keyword.ace_operator {\
color: #ff4e8a\
}\
\
.ace-vs_dark .ace_constant.ace_language {\
color: #FFEE99\
}\
\
.ace-vs_dark .ace_constant.ace_numeric {\
color: #b5cea8\
}\
\
.ace-vs_dark .ace_constant.ace_character {\
color: #FFEE99\
}\
\
.ace-vs_dark .ace_constant.ace_character.ace_escape {\
color: #95E6CB\
}\
\
.ace-vs_dark .ace_constant.ace_other {\
color: #FFEE99\
}\
\
.ace-vs_dark .ace_support.ace_function {\
color: #569cd6\
}\
\
.ace-vs_dark .ace_support.ace_constant {\
font-style: italic;\
color: #569cd6\
}\
\
.ace-vs_dark .ace_support.ace_class {\
font-style: italic;\
color: #569cd6\
}\
\
.ace-vs_dark .ace_support.ace_type {\
font-style: italic;\
color: #39BAE6\
}\
\
.ace-vs_dark .ace_storage {\
color: #569cd6\
}\
\
.ace-vs_dark .ace_storage.ace_type {\
color: #569cd6\
}\
\
.ace-vs_dark .ace_invalid {\
color: #FF3333\
}\
\
.ace-vs_dark .ace_invalid.ace_deprecated {\
color: #FFFFFF;\
background-color: #569cd6\
}\
\
.ace-vs_dark .ace_string {\
color: #ce9178\
}\
\
.ace-vs_dark .ace_string.ace_regexp {\
color: #ce9178\
}\
\
.ace-vs_dark .ace_comment {\
font-style: italic;\
color: #608b4e\
}\
\
.ace-vs_dark .ace_variable {\
color: #fff\
}\
\
.ace-vs_dark .ace_variable.ace_language {\
font-style: italic;\
color: #fff\
}\
\
.ace-vs_dark .ace_variable.ace_parameter {\
color: #b5cea8\
}\
\
.ace-vs_dark .ace_entity.ace_other.ace_attribute-name {\
color: #68bbc5\
}\
\
.ace-vs_dark .ace_entity.ace_name.ace_function {\
color: #fff\
}\
\
.ace-vs_dark .ace_entity.ace_name.ace_tag {\
color: #ff4e8a\
}'

var dom = acequire('../lib/dom')
dom.importCssString(exports.cssText, exports.cssClass)
})